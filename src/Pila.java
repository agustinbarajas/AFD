
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author abv17
 */
public class Pila {
    private int tope;//Almacena la posición de la cima de la pila
    private final ArrayList<Object> vector;//Almacena todos los elementos insertados en la pila
    public Pila(){//Constructor
        tope=-1;//Inicializar a tope en -1 para determinar que la pila está vacia
        vector= new ArrayList<>();//Inicializar el arreglo en vacio
    }//Fin de constructor
    public Object elementoCima(){//Método para devolver el elemento que se encuentra en la cima de la pila
        return vector.get(tope);//retorna el elemento de la cima de la pila
    }//Fin de método para regresar el elemento de la cima de la pila
    public boolean insertar(Object elemento){//Método para insertar elementos en la pila
        if(elemento.equals("")){//Verificar que el elemento no este vacio
            return false;//Retornar falso para especificar que no se ha insertado
        }//Fin de condición
        tope++;//Incrementar el tope de la pila
        vector.add(tope, elemento);//Insertar el elemento en la pila
        return true;//Retornar verdadero para determinar que si se ha podido insertar en la pila
    }//Fin del método para insertar en la pila
    public boolean eliminar(){//Método para eliminar el elemento de la cima de la pila
        if(estaVacia()){//Verificar si la pila está vacia
            return false;//Retornar que no se pudo eliminar el elemento puesto que la pila está vacia
        }//Fin de condición
        tope--;//Disminuir el valor de tope para borrar logicamente el ultimo elemento de la pila
        return true;//Retornar verdadero para especificar que si se elimino de la pila
    }//Fin de método para eliminar de  la pila
    public boolean estaVacia(){//Método para determinar si el la pila está vacia
        return tope==-1;//retornar si tope esta o no en -1
    }//fin de método para especificar si la pila está vacia
}