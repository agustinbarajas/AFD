/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author abv17
 */
public class Estado {
    String nombre;//Nombre del estado
    boolean estFin;//Se activa si el estado es un estado final
    boolean estIni;//Se activa si el estado es un estado inicial
    
    Estado(String nom, boolean ef, boolean ei){//Constructor
        nombre = nom;//Inicializar el nombre del estado
        estFin = ef;//Especificar si el estado es o no un estado final
        estIni = ei;//Especificar si el estado es o no un estaod inicial
    }//Fin constructor
}//Fin de clase Estado