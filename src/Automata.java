
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author abv17
 */
public class Automata {
    Notaciones notaciones;//Declarar notaciones para acceder al método para obtener la expresión de forma postorden
    Estado q0, q1, q2, q3, q4, q5;//Declaración de todos los estados del autómata
    Estado estadoActual;//Declaración del estado en el cual se encuentra el seguimiento del autómata
    boolean error;//Daclaración de la variable que almacena si existe o no algún error
    ArrayList<Transicion> tablaTransiciones;//Declaración del arreglo que contendra la tabla de transición
    Automata(){//COnstructor
        q0 = new Estado("q0", false, true);//Inicializar el estado q0, el cual es el estado inicial
        q1 = new Estado("q1", true, false);//Inicializar el estado q1, el cual es un estado final
        q2 = new Estado("q2", false, false);//Inicializar el estado q2, el cual es un estado intermedio
        q3 = new Estado("q3", false, false);//Inicializar el estado q3, el cual es un estado intermedio
        q4 = new Estado("q4", true, false);//Inicializar el estado q4, el cual es un estado final
        q5 = new Estado("q5", true, false);//Inicializar el estado q5, el cual es un estado final
    }//fin de constructor
    
    protected ArrayList<Transicion> validarExpresion(String cadEntrada){//Método para realizar la validación de la expresión aritmética introducida
        if (cadEntrada.isEmpty()) {//Verificar que la cadena de entrada no este vacia
            return null;//Si es vacia, retornar un valor null para control en la vista
        }//fin condición
        notaciones = new Notaciones();//Incializar el objeto para hacer uso de los métodos 
        estadoActual = q0;//Inicializar el estado actual con el estado inicial
        error = false;//Inicializar el error en falso para indicar que no existe ningún error
        tablaTransiciones = new ArrayList<>();//Inicializar el arraylist que contendrá todas las transiciones realizadas entre los estados
        ArrayList<Object> elementosExp = notaciones.postorden(cadEntrada);//Obtener en un arraylist todos los elementos de la cadena de entrada
        String elemento;//Declarar la variable que almacenará si el elemento de entrada es un operando o un operador
        for (int i = 0; i < elementosExp.size(); i++) {//For para recorrer el arraylist que contiene todos los elementos de la cadena de entrada
            elemento =esOperadorUOperando(elementosExp.get(i).toString());//Asignar si el término analizado es un operador o un operando
            if (!error && !elemento.equals("")) {//Verificar que no existan errores y que el término este correctamente categorizado
                switch(estadoActual.nombre){//Switch para estipular el comportamiento dependiendo del estado en el que se encuentre
                    case "q0"://Caso para cuando el estado es q0
                        if (elemento.equals("Operando")) {//Verificar si el término es un operando
                            tablaTransiciones.add(new Transicion(q0,q1,"Operando ("+elementosExp.get(i)+")"));//Agregar la transición a la tabla
                            estadoActual= q1;//posicionar a q1 como el estado actual
                            break;//Cortar la ejecución y analizar el siguiente término
                        }else
                        if (elemento.equals("Operador")) {//Verificar si el término es un operador
                            tablaTransiciones.add(new Transicion(q0,q2,"Operador ("+elementosExp.get(i)+")"));//Agregar la transición correspondiente a la tabla
                            estadoActual= q2;//Asignar a q2 como estado actual
                            break;//Cortar la ejecución y analizar el siguiente término
                        }else{
                            error= true;//Activar la bandera del error a verdadero
                        }//fin del else
                    case "q1"://Caso para cuando el estado es q1
                        if (elemento.equals("Operando")) {//Verificar si el término es un operando
                            tablaTransiciones.add(new Transicion(q1,q3,"Operando ("+elementosExp.get(i)+")"));//Agregar la transición correspondiente a la tabla
                            estadoActual= q3;//Asignar a q3 como estado actual
                            break;//Cortar la ejecución y analizar el siguiente término
                        }else
                        if (elemento.equals("Operador")) {//Verificar que es término sea un operador
                            tablaTransiciones.add(new Transicion(q1,q2,"Operador ("+elementosExp.get(i)+")"));//Agregar la transición correspondiente a la tabla
                            estadoActual= q2;//Cambiar el estado actual a q2
                            break;//Cortar la ejecución y analizar el siguiente término
                        }else{
                            error = true;//Activar la bandera del error a verdadero
                        }//fin del else
                    case "q2"://Caso para cuendo el estado es q2
                        if (elemento.equals("Operando")) {//Verificar que el término sea un operando
                            tablaTransiciones.add(new Transicion(q2,q2,"Operando ("+elementosExp.get(i)+")"));//Agregar la transición correspondiente a la tabla
                            estadoActual= q2;//Cambiar el estado actual a q2
                            break;//Cortar la ejecución y analizar el siguiente término
                        }else
                        if (elemento.equals("Operador")) {//Verificar que el término sea un operador
                            tablaTransiciones.add(new Transicion(q2,q2,"Operador ("+elementosExp.get(i)+")"));//Agregar la transición correspondiente a la tabla
                            estadoActual= q2;//Cambiar el estado actual a q2
                            break;//Cortar la ejecución y analizar el siguiente término
                        }else{
                            error= true;//Activar la bandera del error a verdadero
                        }//fin del else
                    case "q3"://Caso para cuendo el estado es q3
                        if (elemento.equals("Operando")) {//Verificar que el término sea un operando
                            tablaTransiciones.add(new Transicion(q3,q3,"Operando ("+elementosExp.get(i)+")"));//Agregar la transición correspondiente a la tabla
                            estadoActual= q3;//Cambiar el estado actual a q3
                            break;//Cortar la ejecución y analizar el siguiente término
                        }else
                        if (elemento.equals("Operador")) {//Verificar que el término sea un operador
                            tablaTransiciones.add(new Transicion(q3,q4,"Operador ("+elementosExp.get(i)+")"));//Agregar la transición correspondiente a la tabla
                            estadoActual= q4;//Cambiar el estado actual a q4
                            break;//Cortar la ejecución y analizar el siguiente término
                        }else{
                            error= true;//Activar la bandera del error a verdadero
                        }//fin del else
                    case "q4"://Caso para cuando el estado es q4
                        if (elemento.equals("Operando")) {//Verificar que el término sea un operando
                            tablaTransiciones.add(new Transicion(q4,q3,"Operando ("+elementosExp.get(i)+")"));//Agregar la transición correspondiente a la tabla
                            estadoActual= q3;//Cambiar el estado actual a q3
                            break;//Cortar la ejecución y analizar el siguiente término
                        }else
                        if (elemento.equals("Operador")) {//Verificar que el término sea un operador
                            tablaTransiciones.add(new Transicion(q4,q5,"Operador ("+elementosExp.get(i)+")"));//Agregar la transición correspondiente a la tabla
                            estadoActual= q5;//Cambiar el estado actual a q5
                            break;//Cortar la ejecución y analizar el siguiente término
                        }else{
                            error= true;//Activar la bandera del error a verdadero
                        }//fin del else
                    case "q5"://Caso para cuando el estado es q5
                        if (elemento.equals("Operando")) {//Verificar si el término es un operando
                            tablaTransiciones.add(new Transicion(q5,q4,"Operando ("+elementosExp.get(i)+")"));//Agregar la transición correspondiente a la tabla
                            estadoActual= q4;//Cambiar el estado actual a q4
                            break;//Cortar la ejecución y analizar el siguiente término
                        }else
                        if (elemento.equals("Operador")) {//Verificar si el término es un operador
                            tablaTransiciones.add(new Transicion(q5,q5,"Operador ("+elementosExp.get(i)+")"));//Agregar la transición correspondiente a la tabla
                            estadoActual= q5;//Cambiar el estado actual a q5
                            break;//Cortar la ejecución y analizar el siguiente término
                        }else{
                            error= true;//Activar la bandera del error a verdadero
                        }//fin del else
                }//Fin del switch
            }else{
                return tablaTransiciones;//Retornar la tabla de transiciones como esta al momento del error
            }//fin de else
        }//Fin del for
        return tablaTransiciones;//Retornar toda la tabla de transiciones
    }//FIn de método para validar la expresión
    
    protected boolean expCorrecta(){//Método para verificar si una expresión es o no correcta
        if (estadoActual.estFin && !error && !notaciones.error) {//Verificar que el estado actual sea un estado final y que no existan errores
            return true;//retornar verdadero para saber que la expresión es correcta
        }
        return false;//Retornar falso para indicar que la expresión es incorrecta
    }
    
    private String esOperadorUOperando(String termino){//Método para indicar si un término es operando u operador
        if (termino.isEmpty()) {//Verificar que el término no este vacio
            return "";//Retornar vacio para indicar que no hay término que analizar
        }
        if (notaciones.isNumber(termino) || notaciones.esVariable(termino)) {//Verificar si el término es un número o una variable
            return "Operando";//Retornar que el término es un operando
        }
        if (notaciones.esOperador(termino)) {//verificar que el término es un operador
            return "Operador";//retornar que el término analizado es un operador
        }
        return "";//Retornar vacio para indicar que el término no es operador y tampoco operando
    }
}