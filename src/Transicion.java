/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author abv17
 */
public class Transicion {
    Estado estadoIni;//Estado del cual inicia la transición
    Estado estadoFin;//Estado al cual va la transición
    String simbolo;//Elemento con el cual se realiza la transición
    
    Transicion(Estado ei, Estado ef, String sim){//Constructor
        estadoIni = ei;//Inicializar el estado de origen
        estadoFin = ef;//Inicializar el estado destino
        simbolo = sim;//Inicializar el símbolo o elemento con el cual se realiza la transición
    }//Fin de Constructor
}//Fin de clase Transicion