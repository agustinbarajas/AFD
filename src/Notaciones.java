
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author abv17
 */
public class Notaciones {
    Pila pila; // Pila de apoyo para realizar notación postfija
    String cadSalida; //Cadena de salida con la expresión en notación postfija
    boolean error; //Almacena si existe un error en la conversión a notación postfija
    ArrayList<Object> elementos;//Declarar arreglo que contendra los elementos de la cadena de entrada
    
    public ArrayList<Object> postorden(String cadEntrada){//Método para llevar a cabo la notación postfija
        if (cadEntrada.isEmpty()) {//Verificar si la cadena de entrada esta vacia
            return null; // En caso de estar vacia la cadena de entrada, se retorna vacio en cadena de salida
        }//Fin condición de cadena entrada vacia
        int cantParIzq=0;
        error = false; // Inicializa error en falso
        cadSalida = "";//Inicializar cadena de salida en vacio
        elementos= new ArrayList<>();//Inicializar el arreglo que contendra todos los elementos de la expresión en modo postorden
        pila = new Pila();//Crear una pila vacia
        int pos=0; //Indica en la posición donde va el análisis de la cadena de entrada
        String termino; //Almacena el término actual de la cadena de entrada
        while(!error && pos<cadEntrada.length()){//Indica cuando exista un error o que se haya sobrepasado la cantidad de terminos en la cadena de entrada
            termino = extraerTermino(cadEntrada, pos);//Almacena el término actual en la cadena de entrada en una posición especifica
            if(!termino.isEmpty() && !error){//Indica si el término extraido de la cadena de entrada está o no vacio
                if (isNumber(termino)) {//Verifica si el término actual es un número
                    cadSalida+=termino;//Si es un número lo almacena directamente en la cadena de salida
                    elementos.add(termino);//Agregar elemento al arreglo de los elementos de la expresión en modo postorden
                    pos+=termino.length();//Incrementa la posición en la cantidad de caracteres que tenga el término analizado
                    continue;//Termina la iteración actual e inicia una nueva
                }//Fin de condición para saber si es un número
                if (esVariable(termino)) {//Verificar si el término actual es una variable
                    cadSalida+=termino;//Si es una variable se almacena en el final de la cadena de salida
                    elementos.add(termino);//Agregar elemento al arreglo de los elementos de la expresión en modo postorden
                    pos+=termino.length(); //Incrementa la posición en la cantidad de caracteres que tiene el término actual
                    continue;//Termina la iteración actual e inicia una nueva
                }//Fin de condición para saber si el término actual es una variable
                if (esParentesisAbierto(termino)) {//Verificar si el término actual es un paréntesis abierto
                    if (pos>0 && esOperador(extraerTermino(cadEntrada, pos-1)) && pos==cadEntrada.length()-1) {
                        error = true;//Si existe un error al determinar la prioridad del operador, se activa la variable error
                        return elementos;//Debido a la existencia de un error, se retorna la cadena como estaba antes de ocurrir el error
                    }
                    pila.insertar(termino);//Si es un parentesis abierto, se almacena en la pila
                    pos+=termino.length();//Se incrementa la posición en la cantidad de caracteres que posee el término actual
                    cantParIzq++;
                    continue;//Finaliza la iteración actual e inicia una nueva
                }//Fin de condición para saber si el término es un paréntesis abierto
                if (esParentesisCerrado(termino)) {//Verificar si el término actual es un paréntesis cerrado
                    cantParIzq--;
                    if (cantParIzq<0) {
                        error =true;//Activa el error en verdadero para dejar de iterar
                        return elementos;//retorna la cadena de salida como quedó antes del error
                    }
                    while(!pila.estaVacia() && !esParentesisAbierto(pila.elementoCima().toString())){//Verificar si la pila no esta vacia y si el tope de la pila no es un paréntesis abierto
                        String elemento = pila.elementoCima().toString();//Almacena el termino almacenado en el tope de la pila
                        cadSalida+=elemento;//Agrega el elemento en el tope de la pila al final de la cadena de salida
                        elementos.add(elemento);//Agregar elemento al arreglo de los elementos de la expresión en modo postorden
                        pila.eliminar();//Quita el último elemento de la pila
                    }//Fin de ciclo para extraer elementos de la pila que no sean paréntesis abiertos
                    if (esParentesisAbierto(pila.elementoCima().toString()) && !pila.estaVacia()){//Verifica si en el tope de la pila está un paréntesis abierto
                        pila.eliminar();//Elimina el elemento en el tope de la pila
                    }else{//Si no existe algún paréntesis de apertura en la pila, significa que hay un error
                        error =true;//Activa el error en verdadero para dejar de iterar
                        return elementos;//retorna la cadena de salida como quedó antes del error
                    }//Fin de la valoración sobre si hay o no un error
                    if (pos==cadEntrada.length()-1 && cantParIzq!=0 || esOperador(extraerTermino(cadEntrada, pos-1))) {
                        error =true;//Activa el error en verdadero para dejar de iterar
                        return elementos;//retorna la cadena de salida como quedó antes del error
                    }
                    pos+=termino.length();//Incrementa la posición en la cantidad de caracteres que posee el término actual
                    continue;//Termina la iteración actual e inicia una nueva
                }//Fin de condición para verificar si el término es un paréntesis cerrado
                if (esOperador(termino)) {//Verificar si el término actual es un operador
                    if (pos==0) {
                        error = true;//Si existe un error al determinar la prioridad del operador, se activa la variable error
                        return elementos;//Debido a la existencia de un error, se retorna la cadena como estaba antes de ocurrir el error
                    }
                    int prioridadOp= prioridadOperador(termino);//Almacena la prioridad del operador actual
                    if (prioridadOp==0 || esOperador(extraerTermino(cadEntrada, pos-1)) || pos+1==cadEntrada.length()) {//Verifica que no haya ocurrido algún error al determinar la prioridad del operador
                        error = true;//Si existe un error al determinar la prioridad del operador, se activa la variable error
                        return elementos;//Debido a la existencia de un error, se retorna la cadena como estaba antes de ocurrir el error
                    }//Fin de verificación de la existencia de un error en la determinación de la prioridad
                    while(!pila.estaVacia() && prioridadOp<=prioridadOperador(pila.elementoCima().toString())){//Determinar si el operador actual tiene menor o igual prioridad que el que se encuentra en la pila
                        String elemento = pila.elementoCima().toString();//Extraer elemento ubicado en el tope de la pila
                        cadSalida+=elemento;//Insertar el elemento recuperado de la pila en el final de la cadena de salida
                        elementos.add(elemento);//Agregar elemento al arreglo de los elementos de la expresión en modo postorden
                        pila.eliminar();//Eliminar el elemento ubicado en el tope de la pila
                    }//Fin de ciclo para verificar prioridades de los operadores
                    pila.insertar(termino);//Insertar el elemento actual en el tope de la pila
                    pos+=termino.length();//Incrementar la posición en la cantidad de caracteres que tenga el término actual
                }//Fin de verificación si el término actual es un operado
            }//Fin de verificación si el término actual no esta vacio
        }//Fin de verificación si no exite un error o se termino de recorrer la cadena de entrada
        while(!pila.estaVacia() && !error){//Verificar si no existe un error y si la pila no está vacia para extraer los elementos existentes en ella
            String elemento = pila.elementoCima().toString();//Almacena el elemento ubicado en el tope de la pila
            if (esParentesisAbierto(elemento)) {//Verificar si existe un parentesis abierto de más
                error= true;//Activar la variable error para dejar de agregar elementos
                return elementos;//Retornar el arreglo con todos lo elementos validos
            }//Fin de condición de error
            cadSalida+=elemento;//Agrega el elemento del final de la pila al final de la cadena de salida
            elementos.add(elemento);//Agregar elemento al arreglo de los elementos de la expresión en modo postorden
            pila.eliminar();//Elimina el elemento ubicado en el tope de la pila
        }//Fin de extracción de los elementos de la pila
        //pila=null;
        return elementos;//Retornar la cadena de salida construida
    }//Fin del método de conversión a notación Postfija
    private int prioridadOperador(String op){//Método para determinar la prioridad de un operador
        if (op.isEmpty() || op.length()!=1) {//Determina si el parámetro esta vacio o si la cadena tienen más de un caracter
            return 0;//Retorna un número de control para saber si exite un error
        }//Fin de condición para determinar error
        String[][] operadores={{"+","-"},{"*","/"},{"^"}};//Almacena los operadores de acuerdo a su prioridad y si son de prioridades iguales o no
        if (op.equals(operadores[0][0]) || op.equals(operadores[0][1])) {//Determina si el operador es un + o un - 
            return 1;// Asigna la prioridad de 1 para el + o para el -
        }//Fin de asignación si es + o es -
        if (op.equals(operadores[1][0]) || op.equals(operadores[1][1])) {//Determinar si el operador es un * o una /
            return 2;//Asgina la prioridad de 2 para el * o para la /
        }//Fin de asignación si es * o /
        if (op.equals(operadores[2][0])) {//Determina si el operador es ^
            return 3;//Asigna la prioridad de 3 para la ^
        }//Fin de asignación si es ^
        return 0;//Devuelve un valor de control por si no se encontro el operador
    }//Fin del método para la asignación de prioridades a los operadores
    protected boolean esVariable(String caracter){//Método para determinar si un término es una variable
        if (caracter.isEmpty()) {//COndifición para determinar si el parámetro esta vacio
            return false;//Devuelve un valor de control para determinar que no es una variable
        }//Fin de condición para determinar si el parámetro esta vacio
        boolean esVariable=false;//Almacena si el término actual es o no una variable
        if (caracter.charAt(0)>=65 && caracter.charAt(0)<=90 || caracter.charAt(0)>=97 && caracter.charAt(0)<=122) {//Condiciones para determinar si el primer caracter cumple con los requisitos de una variable
            for(int i=0; i<caracter.length();i++){//For para recorrer todos los caracteres del término que se está analizando
                esVariable = caracter.charAt(i)>=65 && caracter.charAt(i)<=90 || caracter.charAt(i)>=97 && caracter.charAt(i)<=122 || isNumber(caracter) || caracter.charAt(i)==95;//Determinar si el resto de los caracteres corresponden con una variable
            }//Fin del for para el análisis de los caracateres del término
        }//Fin de la condición
        return esVariable;//Retornar si es o no una variable
    }//FIn del método para determinar si es o no una variable
    protected boolean isNumber(String character){//Método para determinar si el término es un número
        if (character.isEmpty()) {//Verificar si el parámetro no esta vacio
            return false;//Devolver falso como contro para determinar que pasó un error
        }//FIn de condición 
        boolean isNumber = false;//Almacena si es o no un número el término analizado
        for (int i = 0; i < character.length(); i++) {//For para recorrer todos los caracteres del término
            isNumber = character.charAt(i)>=48 && character.charAt(i)<=57 || character.charAt(i)==46;//Determinar si cumple con los requisitos para ser un número
        }//Fin de for
        return isNumber;//Retornar si es o no un número
    }//Fin de método para determinar si es o no un número
    private boolean esParentesisAbierto(String caracter){//Método para deteriminar si el elemento es un paréntesis abierto
        if (caracter.isEmpty() || caracter.length()!=1) {//Determinar si el parámetro no esta vacio y si tiene solo un cáracter
            return false;//Regresar falso puesto que esta vacio o porque no tiene solo un cáracter
        }//fin de condición
        return caracter.charAt(0)==40;//Retornar si es o no paréntesis abierto
    }//FIn de metodo para determinar si es un paréntesis abierto
    private boolean esParentesisCerrado(String caracter){//Método para determinar si el término es o no un paréntesis cerrado
        if (caracter.isEmpty() || caracter.length()!=1) {//Determinar que el parámetro no este vacio y que solo contenga un cáracter
            return false;//Regresar falso porque esta vacio o porque no tiene solo un cáracter
        }//Fin de condición
        return caracter.charAt(0)==41;//Regresar si es o no un paréntesis cerrado
    }//Fin del método para determinar si es o no un paréntesis cerrado
    protected boolean esOperador(String caracter){//Método para determinar si el término es o no un operador
        if (caracter.isEmpty() || caracter.length()!=1) {//Verificar si el parámetro esta vacio o si tiene más de un cáracter
            return false;//Regresar falso puesto que no se cumple con los requisitos mínimos
        }//Fin de condición
        String[] operadores = {"+","-","*","/","^"};//Arreglo que contiene todo los posibles operadores 
        for(String op : operadores){//For-each para analizar los operadores
            if (caracter.equals(op)) {//Verificar si el término es un operador
                return true;//Devuelve verdadero si el término se encuentra en los operadores
            }//Fin de condición
        }//Fin de for-each
        return false;//Retornar falso para determinar que no es un operador
    }//Fin de método para determinar si es o no un operador
    private String extraerTermino(String cadE, int pos){//Método para extraer el siguiente término de la cadena de entrada
        if (cadE.isEmpty() || pos>=cadE.length()) {//Determinar si la cadena esta o no vacia
            return "";//Si esta vacia salir y retornar cadena vacia
        }//Fin de condición
        String carAct;//Variable que almacena el caracter actual de la cadena de entrada
        char[] operadores ={'+','-','*','/','^'};//Arreglo que contiene todos los operadores posibles
        char caracter = (char)cadE.charAt(pos);//Almacena el caracter en la posición actual
        if (cadE.charAt(pos)== 40 || cadE.charAt(pos)==41) {//Determinar si el caracter es un parentesis de entrada o de salida
            return caracter+"";//retornar el caracter
        }//fin de condición
        for(char op : operadores){//For-each para determinar si el cáracter es un operador
            if (caracter==op) {//Verificar si es un operador
                return caracter+"";//Retornar el operador
            }//fin de condición
        }//FIn de for-each
        int posEnd= poSigOp(cadE, pos);//Almacena la posición del siguiente operador de la cadena de entrada
        int posEndPar=cadE.indexOf(")", pos);//Almacena la posición del siguiente paréntesis de cierre
        if (posEndPar!=-1 && posEndPar<((posEnd==-1)?cadE.length():posEnd)) {//Determinar si existe o no un paréntesis más de cierre
            //System.out.println("es subcadena antes de un )");
            carAct = cadE.substring(pos, cadE.indexOf(")", pos));//Extraer la subcadena de la cadena de entrada 
            return carAct;//retornar el término
        }//fin de condición
        if(posEnd!=-1 && posEnd>pos && posEnd<((posEndPar==-1)?cadE.length():posEndPar)){//Determinar si la posición del siguiente operador es menor que la del siguiente paréntesis
            carAct= cadE.substring(pos, posEnd);//Almacenar la subcadena 
            return carAct;//Retornar la subcadena o variable
        }//fin de condición
        carAct= cadE.substring(pos, cadE.length());//Almacena la subcadena desde la posición actual hasta el final de la cadena de entrada
        return carAct;//retornar la subcadena o variable
    }//Fin del método para extraer el siguiente término de la cadena de entrada 
    private int poSigOp(String cE, int p){//Método para determinar la posición del operador inmediatamente consecutivo
        int index=cE.length();//Almacenar la longitud de la cadena de entrada
        if (cE.indexOf("+", p)>p && cE.indexOf("+",p)!=-1) {//Deterinar después existe un operador +
            index = cE.indexOf("+", p);//Si existe se almacena el índice dónde éste se encuentra
        }//Fin de condición para el operador +
        if (cE.indexOf("-", p)<index && cE.indexOf("-",p)!=-1) {//Determinar después existe un operador - y si se encuentra antes que otro operador
            index = cE.indexOf("-",p);//Si el operador existe y está antes que otro, se almacena la posición donde se encuentra
        }//Fin de condición para el operador -
        if (cE.indexOf("*", p)<index && cE.indexOf("*", p)!=-1) {//Determinar si después existe un operador * y si se encuentra antes que los analizados anteriormente
            index = cE.indexOf("*", p);//Almacenar el índice donde se encontro
        }//Fin de condición para el operador *
        if (cE.indexOf("/", p)<index && cE.indexOf("/", p)!=-1) {//Determinar si el operador / existe después y si se encuentra antes que los demás
            index = cE.indexOf("/", p);//Almacenar la posición del operador
        }//Fin de condición para el operador /
        if (cE.indexOf("^", p)<index && cE.indexOf("^", p)!=-1) {//Determinar si el operador ^ existe después y si está antes que los demás
            index = cE.indexOf("^", p);//Almacenar la posición donde se encuentra
        }//Fin de condición para el operador ^
        if (index<cE.length()) {//Determinar si el valor de index cambio o si se mantuvo igua
            return index;//retornar el índice
        }//Fin de condición
        return -1;//Retornar un número de control para determinar que no existe ningún operador después
    }//Fin de método para determinar la posición del siguiente operador en la cadena de entrada
}